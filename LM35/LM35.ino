const int pin=A0;
const int resol=10;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

float LM35Return(int entrada){
  float temp=0.00;
  float volt=0.00;
  volt=entrada*5/pow(2,resol);
  temp=volt/0.01;
  return temp;
}

void loop() {
  Serial.println(LM35Return(analogRead(pin)));

}
