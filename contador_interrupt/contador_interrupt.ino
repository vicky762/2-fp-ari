const int suma=3;
const int resta=2;
unsigned int contador=15;
#include <Wire.h>      // libreria para bus I2C
#include <Adafruit_GFX.h>   // libreria para pantallas graficas
#include <Adafruit_SSD1306.h>   // libreria para controlador SSD1306
 
#define ANCHO 128     // reemplaza ocurrencia de ANCHO por 128
#define ALTO 64       // reemplaza ocurrencia de ALTO por 64

#define OLED_RESET 4      // necesario por la libreria pero no usado
Adafruit_SSD1306 oled(ANCHO, ALTO, &Wire, OLED_RESET);

void setup() {
  pinMode(suma,INPUT_PULLUP);
  pinMode(resta,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(suma),asciende,FALLING);
  attachInterrupt(digitalPinToInterrupt(resta),desciende,FALLING);
  Wire.begin();         // inicializa bus I2C
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.clearDisplay();      // limpia pantalla
  oled.setTextColor(WHITE);   // establece color al unico disponible (pantalla monocromo)
  oled.setCursor(0, 0);     // ubica cursor en inicio de coordenadas 0,0
  oled.setTextSize(2);
  oled.print("Contador: ");
  oled.print(contador);
  oled.display();   
  // put your setup code here, to run once:

}

void asciende(){
  if(contador<40){
    contador++;
  }
  while(digitalRead(suma)==LOW);
}
void desciende(){
  if(contador>15){
    contador--;
  }
  while(digitalRead(suma)==LOW);
}

void loop() {
  oled.clearDisplay();  
  oled.setCursor(0, 0);     // ubica cursor en inicio de coordenadas 0,0
  oled.setTextSize(2);
  oled.print("Contador: ");
  oled.print(contador);
  oled.display();   

}
