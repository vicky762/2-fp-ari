const int pot=3;
const int engine=5;
const int pulse=2;
/*
 * TF98-25IM-74
 * 12V 90 mA
 * 9V 70 mA
 */
unsigned long t0,s0;
unsigned long t1,s1;
float freq=0.00;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pulse,INPUT);
  attachInterrupt(digitalPinToInterrupt(pulse),triger, RISING);
  pinMode(engine,OUTPUT);
  s0=millis();
}
void triger(){
  if(t0==0){
    t0=micros();
  }
  else if(t1==0){
    t1=micros();
  }
}

void calculos(){
  freq=(1/(t1-t0)*1000000);
}

void loop() {
  
  int vmedio=0,pwm=0;
  vmedio=analogRead(3);
  pwm=map(vmedio,0,1023,0,255);
  analogWrite(engine,pwm);
  if(s1-s0==1000){
    Serial.print("T0: ");
    Serial.print(t0);
    Serial.println(" us");
    Serial.print("T1: ");
    Serial.print(t1);
    Serial.println(" us");
    s0=millis();
  }
  if(t1>0&&t0>0){
    freq=1000000.00/(t1-t0);
    Serial.print("Tiempo: ");
    Serial.print(t1-t0);
    Serial.println(" us");
    Serial.print("Frecuencia: ");
    Serial.println(freq,2);
    Serial.println(" Hz");
    t1=0;
    t0=0;
  }
  s1=millis();
}
