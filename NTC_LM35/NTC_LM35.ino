const int LM35=A0;
const int NTC=A1;

const int resol=10;
const float top=5.00;
const float R0=9.95;
const float RT0=10000;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

float LM35Return(int entrada){
  float temp=0.00;
  float volt=0.00;
  volt=entrada*5/pow(2,resol);
  temp=volt/0.01;
  return temp;
}

float NTCReturn(int entrada){
  const float A=1.11492089e-3,B=2.372075385e-4,C=6.954079529e-8;
  float V=0, R=0, Rth=0,Tk=0,T=0;
  V=entrada*top/(pow(2,resol)-1);
  R=(RT0*V)/(5.00-V);
  T = (1.0 / (A + B*log(R) + C*log(R)*log(R)*log(R)));
  T = T - 273.15;
  return T;
}

void loop() {

  if(millis()%1000==0){
    Serial.print("LM35: ");
    Serial.print(LM35Return(analogRead(LM35)));
    Serial.println(" ºC");
    Serial.print("NTC: ");
    Serial.print(NTCReturn(analogRead(NTC)));
    Serial.println(" ºC");
  }

}
