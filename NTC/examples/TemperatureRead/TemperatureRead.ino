#include <NTC.h>
const int pin=A1;

 float A=1.11492089e-3,B=2.372075385e-4,C=6.954079529e-8,vcc=5.00,top=5.00;
 int R=10000,resol=10;
NTC myNTC;



void setup() {
  int f=1;
  while(f!=0){
    f=myNTC.createNTC(A,B,C,top,vcc,2.00,R,resol);
  }
  Serial.begin(9600);
  Serial.println("NTC READY!");
  // put your setup code here, to run once:

}

void loop() {
  float Tc,Tk,Tcor;
  Tk=myNTC.getTemperature(analogRead(A1));
  Tcor=myNTC.getCorrectedT(Tk);
  Tc=myNTC.getTCelsius(Tcor);
  if(millis()%1000==0){
    Serial.print("Kelvin: ");
    Serial.println(Tk);
    Serial.print("Celsius: ");
    Serial.println(Tc);
    Serial.print("Corregida: ");
    Serial.println(Tcor);
  }
  // put your main code here, to run repeatedly:
  
}
