const int suma=22;
const int resta=24;
unsigned int cuenta=15;
const int signa=13;

#include <Wire.h>      // libreria para bus I2C
#include <Adafruit_GFX.h>   // libreria para pantallas graficas
#include <Adafruit_SSD1306.h>   // libreria para controlador SSD1306
 
#define ANCHO 128     // reemplaza ocurrencia de ANCHO por 128
#define ALTO 64       // reemplaza ocurrencia de ALTO por 64

#define OLED_RESET 4      // necesario por la libreria pero no usado
Adafruit_SSD1306 oled(ANCHO, ALTO, &Wire, OLED_RESET);  // crea objeto

void setup() {
  // put your setup code here, to run once:
  pinMode(suma,INPUT_PULLUP);
  pinMode(resta,INPUT_PULLUP);
  pinMode(signa,OUTPUT);
  Serial.begin(9600);
  Wire.begin();         // inicializa bus I2C
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  oled.clearDisplay();      // limpia pantalla
  oled.setTextColor(WHITE);   // establece color al unico disponible (pantalla monocromo)
  oled.setCursor(0, 0);     // ubica cursor en inicio de coordenadas 0,0
  oled.setTextSize(2);
  oled.print("Contador: ");
  oled.print(contador);
  oled.display();   
   
}
/*
 * As it is configurated, HIGH means door closed, LOW means door opened.
 */

byte digitalReadCor(int pin){
  int i;
  const int top=100;
  float medicion[2]={0,0};
  for(i=0;i<top;i++){
    if(digitalRead(pin)==HIGH){
      medicion[0]+=1;
    }
  }
  medicion[1]=medicion[0]/top;
  if((1-medicion[1])<(medicion[1]-0)){
    return 1;
  }
  else{
    return 0;
  }
}
byte ciclo[2]={1,1};

void loop() {
  // put your main code here, to run repeatedly:
  ciclo[0]=digitalReadCor(suma);
  ciclo[1]=digitalReadCor(resta);
  while(digitalRead(suma)==LOW||digitalRead(resta)==LOW);
  //SE ALMACENA EL ESTADO DE AMBOS BOTONES
  if(ciclo[0]!=ciclo[1]){
    //En caso de que ambos estados difieran se procede a comprobar cuál ha sido accionado
    if(ciclo[0]==0&&cuenta<40){
      //Se suma una unidad
      cuenta++;
    }
    if(ciclo[1]==0&&cuenta>15){
      cuenta--;
    }
    
  }
  oled.clearDisplay();      // limpia pantalla
  oled.setTextColor(WHITE);   // establece color al unico disponible (pantalla monocromo)
  oled.setCursor(0, 0);     // ubica cursor en inicio de coordenadas 0,0
  oled.print("Contador: ");
  oled.print(cuenta);
  oled.display();   
  ciclo[0]=1;
  ciclo[1]=1;
}



/*
 * Using a limit switch to count the number of times a door is opened.
 */
