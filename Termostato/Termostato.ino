#include <NTC.h>
const int pin=A1;

const int plus=22,minus=24;
const int term=23;

float A=1.11492089e-3,B=2.372075385e-4,C=6.954079529e-8,vcc=5.00,top=5.00;
int R=10000,resol=10;
float reference=20.00;

NTC myNTC;
void setup() {
  int f=1;
  while(f!=0){
    f=myNTC.createNTC(A,B,C,top,vcc,2.00,R,resol);
  }
  pinMode(plus,INPUT_PULLUP);
  pinMode(minus,INPUT_PULLUP);
  pinMode(term,OUTPUT);
  Serial.begin(9600);
}

void calculateT(float input,float *output){
  float Tc=0, Tk=0;
  Tk=myNTC.getTemperature(input);
  Tk=myNTC.getCorrectedT(Tk);
  Tc=myNTC.getTCelsius(Tk);
  *output=Tc;
}

void operate(float temp,float h,int pin1){
  byte flag=0;
  int _ref=reference,_h=h,_temp=temp;
  if(_ref+_h<_temp&&millis()%500==0){
    digitalWrite(term,LOW);
  }
  else if(millis()%500==0){
    digitalWrite(term,HIGH);
  }
}

byte digitalReadCor(int pin){
  int i;
  const int top=20;
  float medicion[2]={0,0};
  for(i=0;i<top;i++){
    if(digitalRead(pin)==HIGH){
      medicion[0]+=1;
    }
  }
  medicion[1]=medicion[0]/top;
  if((1-medicion[1])<(medicion[1]-0)){
    return 1;
  }
  else{
    return 0;
  }
}

void loop() {
  float T=0.00,Hist=2.00;
  if(digitalReadCor(plus)==0&&reference<40.00){
    reference+=1.00;
    while(digitalRead(plus)==LOW);
  }
  if(digitalReadCor(minus)==0&&reference>15.00){
    reference-=1.00;
    while(digitalRead(minus)==LOW);
  }
  calculateT(analogRead(pin),&T);
  operate(T,Hist,term);
  if(millis()%1000==0){
    Serial.print("Referencia: ");
    Serial.println(reference,0);
    Serial.print("Sensor: ");
    Serial.println(T);
  }
}
